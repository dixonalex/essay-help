# README #

This README would normally document whatever steps are necessary to get your essays done in no-time.

### What is an Essay? ###

* ### Quick summary ###
An essay is a piece of writing on a subject. It doesn't require a deep understanding of the topic but a general idea on the subject is essential to get started. There are different types of essays that may have different requirements and instructions to follow. It becomes awesome when you write a good one complying with all the needs.
* ### How to get started with ###
You have to get started with researching. It is impossible to get a flow in writing when you lack essential information. Today, you have the facilities to get more information on a given topic by searching online. You don't need any book or library near you these days. You can research any topic if you have an internet connection with your PC or smartphone. Once you have the essential information, you can start preparing an outline of your essay by arranging the data you have just gotten. It helps you to keep on track preventing you not to go beyond the topic. Start writing the rough draft only after creating a good outline.
* [More About Essay Writing](https://essayschief.com/service/essay/essay-writing)